# Gamil Backup


## Quick Start

The code is generally developed and tested in the Python 2.7.x and Python 3
.6 in Mac OS X system (it should generally in Windows as well).

And before running the programm, please put the `client_secret.json` into the root directory of the repo.

**Note**: to ease the trouble, I'm sending my `client_secret.json` in the Email. I will delete this credential later after the codeview is done.

### In Mac OS X with Python 2.7.10

In general, it is recommended to run the code in the `virtualenv`. 

```bash
source venv/bin/activate
pip install -r requirements.txt
python gmail_sync.py qudong452@gmail -p 2018/03/01 2018/03/05
```

Or with a customized label for the Daemon to push the encrypt Email back to the Mailbox

```bash
python gmail_sync.py qudong452@gmail 2018/03/01 2018/03/05 -bkl SuperMax3
```

## Gmail API

Generally, the offical `google-api-python-client` package is used to inteface with the Gmail API. And worth to mention that the use of [batch HTTP requests](https://developers.google.com/gmail/api/guides/batch) significantly improves the Email message retrieving processing. 

> Each HTTP connection that your client makes results in a certain amount of overhead. The Gmail API supports batching, to allow your client to put several API calls into a single HTTP request.



## Concurrency

### Parallel Programming (Creating Thread Pool) - Choice of choosing the Way to tangle the Problem

Since each thread need need to perform both of the CPU-intensive work (Encryption of the Email) and I/O-intensive works (store the encrypt email in the folder), so the the GIl need to be taken into the consideration (actually in the end the answer is not). As generally the thread pool (`import threading`) generally is good for I/O-bound processing. To perform the Encryption, In order to stop worrying about the GIL and unleash the power of multi CPU cores, the most straightforward way is to work it entirely in Python by using `mulprocessing` or even higher level package `conccurent.futures` to create a process pool and use it like a co-processor *(another way is to foucs on C extension, which is too much effort in this case and importantly overkill)*. 

One subtle approach of pool is that mixing threads and process pools. However great care should be made when combining process pools and programs. In particular, the process pools probably should be created and launched prior to the creation of any threads (e.g., create the pool in the main thread as a singleton at program startup), threads will then use the same process pool for all of their computationally intensive work. However, it is an overkill as well in this case.

Actually, by careful choice of the underlying algorithm may procues a far greater speedup than trying to parallelize an uniptimal algorithm with using the multi-processing! That is why in my program, I'm actually just using the 'threading' by choosing the optimized encrypthon algorithm by using the package `PyCryptodome`.

### Benchmark to compare the Multi-threading and Multi-process with Different Encryption Algorithm

- `PyCryptodome` with multi-threading (`MultiEncryptIOThread` class)

    *(MainThread) total time taken to encrypt & save to file for 14 emails is
     0.0361230373383 sec*

- `PyCryptodome` algorithm with multi-processing (`futures.ProcessPoolExecutor(max_workers=10)`)

    *(MainThread) total time taken to encrypt & save to file for 14 emails is
     0.0952758789062 sec*

- Naive "byte-by-byte" algorithm with multi-threading (`MultiEncryptIOThread` class)

    *(MainThread) total time taken to encrypt & save to file for 14 emails is
     2.37613916397 sec*

- Naive "byte-by-byte" algorithm with multi-processing (`futures.ProcessPoolExecutor(max_workers=10)`)

    *(MainThread) total time taken to encrypt & save to file for 14 emails is
     0.376559972763 sec*


#### Analysis of the Benchmark result

As we can see, actually with the use of `PyCryptodome`, the performance is better when using the multi-thread rather than using the multi-processing (~2x faster with 14 emails); while interestingly with the use of the naive Encryption "byte-by-byte" algorithm, the multi-processsing is faster compared to the multi-threading (~6x faster, I'm running in 2.3 GHz Intel Core i5 CPU, which has 2 core and 4 Threads). But overall, the choise of using `PyCryptodome` procues a far greater speedup than using the naive Encrpytion algorithm!

In Generally, in terms of the overall speed,

**`PyCryptodome` with multi-threading > `PyCryptodome` algorithm with multi-processing >>>> Naive "byte-by-byte" algorithm with multi-processing > Naive "byte-by-byte" algorithm with multi-threading**

And based on my analysis, the benchmark result makes a lot of sense. As when using the `PyCryptodome`, the citical CPU-intensive works are done in the C-extension, the "encrypt and IO thread" becomes more I/O-bounded, in this case, the threading performs better. While when I use the Navive "byte-by-byte" algorithm, large amount of the CPU-intensive need to be perfrom in the pure python code, which in this case the "encrpy and IO thread" is more CPU-bounded, and it performs better with the multiprocessing when all of the CPU cores are enabled.


### The Daemon thread

Generally, the communication between the "Encryption and IO thread" pool and the daemon thread follows the "producer" and "consumer" model. Perhaps the safest way to send data among threads is to use a `Queue` (as `Queue` instances already have all of the required locking, so it is thread safe). Generally, in this case, there are multiple producers and only one consumer.

The tricky part when I implement the Daemon thread is to coordinate the shutdown of the producers and consumer. Because Thread communication with a queue is a one-way and nondeterministic process. Generally, `task_done()` and `join()` of the `Queue` object are used to shutdown the daemon thread and exist from program. Otherwise the main thread will directly lead the exit of the program and the daemon cannot finish pushing the mail.

## Encryption

The Encryption algorithm is mainly enpowered by the [PyCryptodome](https://github.com/Legrandin/pycryptodome). Generally, the package is chosen mainly becuase of its speed (Accelerated AES on Intel platforms via AES-NI; **the extremely critical pieces are implemented as C extensions**). 

> PyCryptodome is not a wrapper to a separate C library like OpenSSL. To the largest possible extent, algorithms are implemented in pure Python. Only the pieces that are extremely critical to performance (e.g. block ciphers) are implemented as C extensions.


Generally, AES (Advanced Encryption Standard) is a symmetric block cipher standardized by NIST. It has a fixed data block size of 16 bytes. Its keys can be 128, 192, or 256 bits long. AES is very fast and secure, and it is the de facto standard for symmetric encryption. 

For the choice of the block cipher mode of operation, `CFB` (*Cipher Feedback*) is chosen because of the two advantages:

> the block cipher is only ever used in the encrypting direction, and the message does not need to be padded to a multiple of the cipher block size (though ciphertext stealing can also be used to make padding unnecessary).


## Future Work

1. More customize way for retrieving the Email from the Mailbox
2. Save the encrypt data into a database instead of the current plain text format...
3. Store the encrpt token after the user grant the access, currently it is directly saved as json.
4. Try to improve the speed of the upload speed of the Deamon thread when pushing the encrypt mail back to the Mailbox.




















