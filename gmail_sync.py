from __future__ import print_function
import httplib2
from apiclient import errors
import json
import time
import os
from apiclient import discovery
from apiclient.http import BatchHttpRequest
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from email.mime.text import MIMEText

import multiprocessing
import threading
from concurrent import futures
import threading

from Crypto import Random
from Crypto.Cipher import AES
import base64
import logging
import sys
import argparse

is_py2 = sys.version[0] == '2'  # added py2&3 compatibility
if is_py2:
    import Queue as Queue
else:
    import queue as Queue

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-10s) %(message)s',
                    )


def parse_args(args):
    """
    Argument parse function

    :param args: the sys.argv except the first element, which represent the
    script name
    :return: parsed arguments
    """
    parser = argparse.ArgumentParser(parents=[tools.argparser])

    parser.add_argument('email', action='store', default='empty_email',
                        help='gmail to backup. xxx@gmail.com')
    parser.add_argument("-p", "--passwd",
                        help="use interactive password authentication. (not recommended)",
                        action='store_const', dest="passwd", const='empty',
                        default='not_seen')

    parser.add_argument('startdate', action='store', default='2018/03/01',
                        help='start date of the email retrieval. in the format of YYYY/MM/DD')
    parser.add_argument('enddate', action='store', default='2018/03/05',
                        help='end date of the email retrieval. in the format '
                             'of YYYY/MM/DD')
    parser.add_argument('-bkl', action='store', dest='backuplabelname',
                        default='Backup_Python',
                        help='Backup Label when the Daemon thread is '
                             'uploading the encrypt Email')
    return parser.parse_args(args)


parserargs = parse_args(sys.argv[1:])
username, pwd, rt_start, rt_end, backuplabelname = parserargs.email, \
                                                   parserargs.passwd, \
                                                   parserargs.startdate, \
                                                   parserargs.enddate, \
                                                   parserargs.backuplabelname
username = username.split("@")[0]
logging.debug('Retrieving Email from account {} from {} to {} and with '
              'the backup Label: {}'.format(
    username, rt_start, rt_end, backuplabelname))
del parserargs.email
del parserargs.passwd
del parserargs.startdate
del parserargs.enddate
del parserargs.backuplabelname
parserargs.logging_level = "DEBUG"
flags = parserargs

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/gmail-python-quickstart.json
SCOPES = ['https://www.googleapis.com/auth/gmail.insert',
          'https://www.googleapis.com/auth/gmail.modify',
          'https://www.googleapis.com/auth/gmail.readonly']
CURR_DIR = os.path.abspath(os.path.dirname(__file__))
CLIENT_SECRET_FILE = os.path.join(CURR_DIR, 'client_secret.json')
APPLICATION_NAME = 'Gmail Multi Encrypt IO Thread Console App'

AKEY = 'mysixteenbytekey'  # AES key must be either 16, 24, or 32 bytes long
if not is_py2:
    AKEY = bytes(AKEY, encoding='utf8')

iv = Random.new().read(AES.block_size)


def encode(AKEY, message):
    cipher = AES.new(AKEY, AES.MODE_CFB, iv)
    if not is_py2:
        message = bytes(message, encoding='utf8')
    crypt = base64.urlsafe_b64encode(iv + cipher.encrypt(message))
    if not is_py2:
        # print('crypt bytes is {}'.format(crypt))
        crypt = crypt.decode('utf8')
    return crypt


def decode(AKEY, enc):
    obj2 = AES.new(AKEY, AES.MODE_CFB, iv)
    decrypt = obj2.decrypt(base64.urlsafe_b64decode(enc)[16:])
    if not is_py2:
        decrypt = decrypt.decode('utf8')
    return decrypt


def encode_slow(key, clear):
    """
    Naive Naive "byte-by-byte" algorithm for benckmark purpose
    """
    enc = []
    for i in range(len(clear)):
        key_c = key[i % len(key)]
        enc_c = chr((ord(clear[i]) + ord(key_c)) % 256)
        enc.append(enc_c)
    return base64.urlsafe_b64encode("".join(enc))


def decode_slow(key, enc):
    """
    Naive Naive "byte-by-byte" algorithm for benckmark purpose
    """
    dec = []
    enc = base64.urlsafe_b64decode(enc)
    for i in range(len(enc)):
        key_c = key[i % len(key)]
        dec_c = chr((256 + ord(enc[i]) - ord(key_c)) % 256)
        dec.append(dec_c)
    return "".join(dec)


def get_credentials(email_addr):
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Args:
      email_addr: Email addr in the string format
    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   '{}-gmail-credential.json'.format(
                                       email_addr))

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else:  # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        logging.debug('Storing credentials to ' + credential_path)
    return credentials


def ListMessagesMatchingQuery(service, user_id, query=''):
    """List all Messages of the user's mailbox matching the query.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      query: String used to filter messages returned.
      Eg.- 'from:user@some_domain.com' for Messages from a particular sender.

    Returns:
      List of Messages that match the criteria of the query. Note that the
      returned list contains Message IDs, you must use get with the
      appropriate ID to get the details of a Message.
    """
    try:
        response = service.users().messages().list(userId=user_id,
                                                   q=query).execute()
        messages = []
        if 'messages' in response:
            messages.extend(response['messages'])

        while 'nextPageToken' in response:
            page_token = response['nextPageToken']
            response = service.users().messages().list(userId=user_id, q=query,
                                                       pageToken=page_token).execute()
            messages.extend(response['messages'])

        return messages
    except errors.HttpError as error:
        print('An error occurred: %s' % error)


def ListLabels(service, user_id):
    """Get a list all labels in the user's mailbox.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.

    Returns:
      A list all Labels in the user's mailbox.
    """
    try:
        response = service.users().labels().list(userId=user_id).execute()
        labels = response['labels']
        return labels
    except errors.HttpError as error:
        print('An error occurred: %s' % error)


def MakeLabel(label_name, mlv='show', llv='labelShow'):
    """Create Label object.

    Args:
      label_name: The name of the Label.
      mlv: Message list visibility, show/hide.
      llv: Label list visibility, labelShow/labelHide.

    Returns:
      Created Label.
    """
    label = {'messageListVisibility': mlv,
             'name': label_name,
             'labelListVisibility': llv}
    return label


def CreateLabel(service, user_id, label_object):
    """Creates a new label within user's mailbox, also prints Label ID.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      label_object: label to be added.

    Returns:
      Created Label.
    """
    try:
        label = service.users().labels().create(userId=user_id,
                                                body=label_object).execute()
        # print(label['id'])
        return label
    except errors.HttpError as error:
        print('An error occurred: %s' % error)


def GetNewLabelID(service, user_id, labelname):
    """
    Given a label name, return the label ID of the given label
    if the labelname is already in the current label list, just get the Label ID
    of it and return; if the lablename is not in the current label list, add a
    new label with the given labelname and return the label ID.

    :param service: Authorized Gmail API service instance.
    :param user_id: User's email address. The special value "me"
    :param labelname: the name of the label
    :return:
    """
    labels = ListLabels(service, user_id)
    labelsName = [lbl['name'] for lbl in labels]
    # print(labelsName)
    # print(labelname)
    if labelname not in labelsName:
        backupLabel = MakeLabel(labelname)
        newlabel = CreateLabel(service, user_id, backupLabel)
        newLabelId = newlabel['id']
    else:
        newLabelId = labels[labelsName.index(backuplabelname)]['id']
    return newLabelId


def GetMessage(service, user_id, msg_id):
    """Get a Message with given ID.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      msg_id: The ID of the Message required.

    Returns:
      A Message.
    """
    try:
        message = service.users().messages().get(userId=user_id,
                                                 id=msg_id).execute()

        logging.debug('Message snippet: %s' % message['snippet'])

        return message
    except errors.HttpError as error:
        print('An error occurred: %s' % error)


def InsertMessage(service, user_id, message):
    """Insert a Message.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      message: Message to insert.

    Returns:
      Message inserted including message id.
    """
    try:
        message = service.users().messages().insert(userId=user_id,
                                                    body=message).execute()
        # print('Message Id: %s' % message['id'])
        return message
    except errors.HttpError as error:
        print('An error occurred: %s' % error)


def CreateMessage(message_text, label, subject, sender=None, to=None):
    """Create a message for an email.

    Args:
      message_text: The text of the email message.
      sender: Email address of the sender.
      to: Email address of the receiver.
      subject: The subject of the email message.

    Returns:
      An object containing a base64 encoded email object.
    """
    message = MIMEText(message_text)
    message['to'] = to
    message['from'] = sender
    message['subject'] = subject
    if is_py2:
        messagestr = message.as_string()
        return {'raw': base64.urlsafe_b64encode(messagestr),
                'labelIds': [label]}
    else:
        messagestr = bytes(message.as_string(), encoding='utf8')
        return {'raw': base64.urlsafe_b64encode(messagestr).decode('utf8'),
                'labelIds': [label]}


def ListMessagesWithinPeriodOfTime(service, user_id, rt_start, rt_end):
    """List a list of Messages IDs within given period of time

    :param service: Authorized Gmail API service instance.
    :param user_id: User's email address. The special value "me"
    :param rt_start: the start of retrieval period, in the format of YYYY/MM/DD
    :param rt_end: the end of retrieval period, in the format of YYYY/MM/DD
    :return: list of Messages IDs
    """
    results = ListMessagesMatchingQuery(service, user_id,
                                        query="in:anywhere after:{} before:{}".format(
                                            rt_start, rt_end))
    ids = [x.get('id') for x in results]
    return ids


def save_encryt_email(id, email_encryt, backupfolder):
    """
    The the Encrypt Emails to the given folder, the file currently is saved
    with the Message ID as the filename; the content is the Encrypt email

    :param id: Email Message ID
    :param email_encryt: Encrypt Email
    :param backupfolder: the backup folder
    :return: None
    """
    with open(os.sep.join([backupfolder, '{}'.format(id)]), 'w') as f:
        f.write(email_encryt)


def encrypt_save_worker(args):
    id, email, backupfolder = args
    email_json = json.dumps(email)
    email_encrypt = encode(AKEY, email_json)
    # email_encrypt = encode_slow(AKEY, email_json)

    save_encryt_email(id, email_encrypt, backupfolder)
    logging.debug('id: {}; email encrypt[:50]: {}'.format(id,
                                                          email_encrypt[:50]))
    return email_encrypt


def make_backup_folder():
    """
    make a backup folder in the same folder as the script with the current
    timestamp

    :return: the directory of the backup folder
    """
    Backupfolder = time.strftime('%Y%m%d_%H-%M-%S-Backup', time.localtime())

    backupfolder = os.sep.join([CURR_DIR, Backupfolder, ''])
    if not os.path.exists(backupfolder):
        os.mkdir(backupfolder)
    return backupfolder


def BatchMessageRetrieve(service, user_id, ids):
    """Batch request to get all the messages of the given ids

    https://developers.google.com/api-client-library/python/guide/batch

    :param service: Authorized Gmail API service instance.
    :param user_id: User's email address. The special value "me"
    :param ids: the list of Message IDs you like to retrieve
    :return: the list of Message
    """
    _messages = []

    def get_message(request_id, response, exception):
        if exception is not None:
            pass
        else:
            logging.debug('Message snippet: %s' % response['snippet'])
            _messages.append(response)

    batch = service.new_batch_http_request()
    for id in ids:
        batch.add(service.users().messages().get(userId=user_id, id=id),
                  callback=get_message)
    batch.execute()
    return _messages


class MultiEncryptIOThread(object):
    def __init__(self, service, function, argsVector,
                 maxThreads=10,
                 daemonThread=True,
                 ):
        """
        Multi-threaded Encryption and IO thread Pool

        :param function: the worker function to encryption each eamil and
        save it to the local directory
        :param argsVector: the args vector for the worker function
        :param maxThreads: the maximum threaded in this thread pool; default
        is 10
        :param daemonThread: to use daemon thread or not;default is True
        """
        self._service = service
        self._function = function
        self._lock = threading.Lock()
        if is_py2:
            self._nextArgs = iter(argsVector).next
        else:
            self._nextArgs = iter(argsVector).__next__
        self._threadPool = [threading.Thread(target=self._doSome)
                            for i in range(maxThreads)]

        self._daemonThread = daemonThread
        if self._daemonThread:
            self._daemon = threading.Thread(target=self._doDaemon)
            self._daemon.setDaemon(True)

        self._queue = Queue.Queue()
        self._print_num = 0

    def _doSome(self):
        while True:
            self._lock.acquire()
            try:
                try:
                    args = self._nextArgs()
                except StopIteration:
                    break
            finally:
                self._lock.release()
                try:
                    result = self._function(args)
                except UnboundLocalError:
                    pass
            if result:
                self._queue.put(result)

    def _DeamonEmailUpload(self,
                           encrypt_email):
        global backupLabelId
        service = self._service
        InsertMessage(service, "me",
                      CreateMessage(encrypt_email, backupLabelId,
                                    None))

    def _doDaemon(self):
        while True:
            head_encrypt_email = self._queue.get()

            self._print_num += 1
            logging.debug(
                'uploading the #{} encrypt email...'.format(
                    self._print_num))
            # store the encrypt email to the mail box
            self._DeamonEmailUpload(head_encrypt_email)

            self._queue.task_done()  # indicate completion, must

    def get(self, *kargs, **kwargs):
        if self._queue is not None:
            return self._queue.get(*kargs, **kwargs)
        else:
            raise ValueError('Not queueing results')

    def start(self):
        if self._daemonThread:
            self._daemon.start()
        for thread in self._threadPool:
            time.sleep(0)  # necessary to give other threads a chance to run
            thread.start()

    def join(self, timeout=None):
        for thread in self._threadPool:
            thread.join(timeout)

    def join_daemon(self):
        if self._daemonThread:
            self._queue.join()  # wait for all encrypt email to be consumed


def main(try_multiprocessing=False):
    credentials = get_credentials(email_addr=username)
    http = credentials.authorize(httplib2.Http())
    # global service  # global because of it is used in Daemon thread
    service = discovery.build('gmail', 'v1', http=http)

    # create backup folder based on the current timestamnp in the current src
    #  folder
    backupfolder = make_backup_folder()

    time_start = time.time()

    # get list of Messages within given period of time
    ids = ListMessagesWithinPeriodOfTime(service, "me", rt_start, rt_end)

    # perform the batch message retrieving
    messages = BatchMessageRetrieve(service, "me", ids)

    time_end = time.time()
    logging.debug(
        "total time taken to retrieve {} emails is {} sec".format(len(ids),
                                                                  time_end - time_start))

    # get backuplabel ID; create new backup label if not exist
    global backupLabelId
    backupLabelId = GetNewLabelID(service, "me", backuplabelname)

    ### Using Multi-thread class - MutilEncryptIOThread
    time_start = time.time()

    # use generator instead of the list comprehension to get the args to ramp
    #  up speed (no saving entire list in memory)
    encrpt_args = ((id, msg_json, backupfolder) for id, msg_json in
                   zip(ids, messages))
    mt = MultiEncryptIOThread(service, encrypt_save_worker, encrpt_args,
                              daemonThread=True)
    mt.start()
    mt.join()
    time_end = time.time()
    logging.debug(
        "total time taken to encrypt & save to file for {} emails is {} sec".format(
            len(ids), time_end - time_start))
    logging.debug("Now Waiting for the daemon thread to be finished...")
    mt.join_daemon()  # waiting for the daemon thread to finish uploading

    #### Using the concurrent.futures
    if try_multiprocessing:
        time_start = time.time()
        with futures.ProcessPoolExecutor(max_workers=10) as pool:
            pool.map(encrypt_save_worker,
                     ((id, msg, backupfolder) for id, msg in
                      zip(ids, messages)))
        time_end = time.time()
        logging.debug(
            "total time taken to encrypt & save to file for {} emails is {} sec".format(
                len(ids), time_end - time_start))


if __name__ == '__main__':
    main()
