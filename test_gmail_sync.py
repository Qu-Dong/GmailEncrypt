import unittest
import os
import sys
import json

sys.argv = ['gmail_sync.py', 'quongnus@gmail', '-p', '2018/02/15',
            '2018/03/01']  # must happen before import gmail_sync
import gmail_sync as gmail
import time
import httplib2


class TestGmailSync(unittest.TestCase):
    def __init__(self, argstest):
        super(TestGmailSync, self).__init__(argstest)

    def _getBatchMessages(self):

        if os.name == 'posix':  # running in Mac
            pass
        else:  # running in windows
            pass
        credentials = gmail.get_credentials(gmail.username)
        http = credentials.authorize(httplib2.Http())
        service = gmail.discovery.build('gmail', 'v1', http=http)

        # create backup folder based on the current timestamnp in the current src
        #  folder
        self.backupfolder = gmail.make_backup_folder()

        rt_start = gmail.rt_start
        rt_end = gmail.rt_end

        # get list of Messages within given period of time
        ids = gmail.ListMessagesWithinPeriodOfTime(service, "me", rt_start,
                                                   rt_end)

        # perform the batch message retrieving
        messages = gmail.BatchMessageRetrieve(service, "me", ids)
        return messages

    def test_parser(self):
        self.assertEquals(gmail.username, 'quongnus')
        self.assertEquals(gmail.pwd, 'empty')
        self.assertEquals(gmail.rt_start, '2018/02/15')
        self.assertEquals(gmail.rt_end, '2018/03/01')
        self.assertEquals(gmail.backuplabelname, 'Backup_Python')

    def test_AESCipher(self):
        messages = self._getBatchMessages()

        test_msg = json.dumps(messages[0])
        print(test_msg)
        crypt = gmail.encode(gmail.AKEY, test_msg)
        test_msg_recovery = gmail.decode(gmail.AKEY, crypt)
        self.assertEquals(test_msg, test_msg_recovery)


if __name__ == '__main__':
    unittest.main()
